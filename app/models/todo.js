var mongoose = require('mongoose');

/*module.exports = mongoose.model('Todo', {
	text : String,
	done : Boolean
});*/

module.exports = mongoose.model('IoTReading', {
	houseid: String,
	usagevol: String,
	Timestamp: String,
	thresholdcrossed : Boolean
});