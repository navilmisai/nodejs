// set up ======================================================================
var express  = require('express');
var app      = express(); 								// create our app w/ express
var mongoose = require('mongoose'); 					// mongoose for mongodb
var mqtt = require('mqtt');

var MongoClient = require('mongodb').MongoClient;

var port  	 = process.env.PORT || 8080; 				// set the port
var database = require('./config/database'); 			// load the database config
var Todo = require('./app/models/todo');
var morgan = require('morgan'); 		// log requests to the console (express4)
var bodyParser = require('body-parser'); 	// pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
// configuration ===============================================================

//var uri = "mongodb+srv://nodeuser:nodeuser@iotcluster-ttjel.mongodb.net/test?retryWrites=true&w=majority";

mongoose.connect(database.url, { useNewUrlParser: true });	// connect to mongoDB database on modulus.io

var options = {
  clientId: 'test-subscriber',
  username: 'iotuser',
  password: 'iotuser'
};

//var client  = mqtt.connect('mqtt://test.mosquitto.org') // sample mqtt broker from mosquitto
var client  = mqtt.connect('mqtt://172.30.185.199',options);
//var client  = mqtt.connect('mqtt://localhost',options);

console.log(client.connected);

app.use(express.static(__dirname + '/public')); 				// set the static files location /public/img will be /img for users
app.use(morgan('dev')); 										// log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'})); 			// parse application/x-www-form-urlencoded
app.use(bodyParser.json()); 									// parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

// routes ======================================================================
require('./app/routes.js')(app);

// mqtt subscription/publication ===============================================
client.on('connect', function () {
  /*client.subscribe('presence', function (err) {
    if (!err) {
      client.publish('presence', 'Hello mqtt');
    }
  })*/
  client.subscribe('home/sensor/water/status', function (err) {
    if (!err) {
      console.log('subscribed successfully - home/sensor/water/status !');
    }
  })
  client.subscribe('home/sensor/water/usage', function (err) {
    if (!err) {
      console.log('subscribed successfully - home/sensor/water/usage !');
    }
  })
})

client.on('message', function (topic, message) {
  // message is Buffer
  var explodedMsg = message.toString().split("|");
  console.log('House ID: ', explodedMsg[0],', Usage Volume: ', explodedMsg[1], ', Timestamp: ', explodedMsg[2]);

  // create a todo, information comes from MQTT Topic
  Todo.create({
    houseid : explodedMsg[0],
	usagevol : explodedMsg[1],
	Timestamp : explodedMsg[2],
	thresholdcrossed : false
  }, function(err, todo) {
           if (err)
               console.log('Error in insert');
  });

  //client.end();
});

// listen (start app with node server.js) ======================================
app.listen(port);
console.log("App listening on port " + port);