var mqtt = require('mqtt');
var mongoose = require('mongoose');                                     // mongoose for mongodb
var MongoClient = require('mongodb').MongoClient;
var database = require('./config/database');                    // load the database config
var Todo = require('./app/models/todo');

mongoose.connect(database.url, { useNewUrlParser: true });      // connect to mongoDB database on modulus.io

var options = {
  clientId: 'test-subscriber'
  //username: 'iotuser',
  //password: 'iotuser'
};

//var client  = mqtt.connect('mqtt://test.mosquitto.org') // sample mqtt broker from mosquitto
//var client  = mqtt.connect('mqtt://172.30.185.199',options);
var client  = mqtt.connect('mqtt://localhost',options);

//console.log(client);

client.on('connect', function () {
  /*client.subscribe('presence', function (err) {
    if (!err) {
      client.publish('presence', 'Hello mqtt');
    }
  })*/
  client.subscribe('home/sensor/water/status', function (err) {
    if (!err) {
      console.log('subscribed successfully - home/sensor/water/status !');
    }
  })
  client.subscribe('home/sensor/water/usage', function (err) {
    if (!err) {
      console.log('subscribed successfully - home/sensor/water/usage !');
    }
  })
})

client.on('message', function (topic, message) {
  // message is Buffer
  var explodedMsg = message.toString().split("|");
  console.log('House ID: ', explodedMsg[0],', Usage Volume: ', explodedMsg[1], ', Timestamp: ', explodedMsg[2]);

// create a todo, information comes from MQTT Topic
  /*Todo.create({
      text : message.toString(),
      done : false
  }, function(err, todo) {
           if (err)
               console.log('Error in insert');
  });*/

  //client.end();
});